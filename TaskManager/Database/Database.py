from firebase import firebase


class Database:
    def __init__(self):
        url = 'https://svv01-ca99a-default-rtdb.firebaseio.com/'
        self.messenger = firebase.FirebaseApplication(url)

    def write(self, doc):
        result = self.messenger.post('/tasks', doc, {'print': 'pretty'})
        return result['name']
