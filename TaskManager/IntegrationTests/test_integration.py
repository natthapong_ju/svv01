import unittest
from TaskManager.Tasks.TaskList import TaskList


class TestTaskList(unittest.TestCase):
    def setUp(self):
        self.taskList = TaskList()

    def test_add(self):
        self.taskList.add("First task")
        self.taskList.add("Second task")
        self.assertEqual(2, len(self.taskList.getTasks()))

    def test_save(self):
        self.taskList.add("First task")
        self.taskList.add("Second task")
        result = self.taskList.save()
        self.assertEqual(2, len(result))
