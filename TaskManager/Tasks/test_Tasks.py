import unittest
from TaskManager.Tasks.TaskList import Task
from TaskManager.Tasks.TaskList import TaskList
from unittest.mock import Mock
from unittest.mock import patch


class TestTask(unittest.TestCase):
    def test_get_dict(self):
        d1 = {'id': 1, 'title': "Some task"}
        t1 = Task(d1['id'], d1['title'])
        self.assertEqual(d1, t1.get_dict(), 'Wrong dict representation')


class TestTaskList(unittest.TestCase):
    def setUp(self):
        self.taskList = TaskList()

    def test_add(self):
        self.taskList.add("First task")
        self.taskList.add("Second task")
        self.assertEqual(2, len(self.taskList.getTasks()))

    def test_save(self):
        with patch('TaskManager.Tasks.TaskList.Database') as MockDatabaseClass:
            mockDb = Mock()
            mockDb.write.side_effect = ['123', '456']
            MockDatabaseClass.return_value = mockDb
            self.taskList = TaskList()
            self.taskList.add("First task")
            self.taskList.add("Second task")
            result = self.taskList.save()
            self.assertEqual(['123', '456'], result)
