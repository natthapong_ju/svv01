from TaskManager.Database.Database import Database


class Task:
    def __init__(self, n, t):
        self.title = t
        self.id = n

    def get_dict(self):
        return {'id': self.id, 'title': self.title}


class TaskList:
    def __init__(self):
        self.taskList = []
        self.lastId = 0
        self.db = Database()

    def add(self, t):
        self.lastId += 1
        self.taskList.append(Task(self.lastId, t))
        return self.lastId

    def remove(self, n):
        for i in range(0, len(self.taskList)):
            if self.taskList[i].id == n:
                self.taskList.pop(i)

    def getTasks(self):
        return self.taskList

    def save(self):
        resultList = []
        for task in self.taskList:
            result = self.db.write(task.get_dict())
            resultList.append(result)
        return resultList
